import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentLayoutComponent } from './shared/layout/content-layout/content-layout.component';
import { AuthGuard } from './shared/guards/auth.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./components/dashboard/dashboard.module').then(
            m => m.DashboardModule
          )
      },
      {
        path: 'accounting',
        loadChildren: () =>
          import('./components/accounting/accounting.module').then(
            m => m.AccountingModule
          )
      },
      {
        path: 'merchandising',
        loadChildren: () =>
          import('./components/merchandising/merchandising.module').then(
            m => m.MerchandisingModule
          )
      },
      // {
      //   path: 'test',
      //   loadChildren: () =>
      //     import('./components/test/test.module').then(
      //       m => m.TestModule
      //     )
      // },
    ]
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./components/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '**',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
