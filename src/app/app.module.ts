import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ModalModule, PaginationModule } from 'ngx-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';
import { SpinnerService } from './shared/service/spinner.service';
import { JwtModule } from '@auth0/angular-jwt';


export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent     
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    SharedModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    JwtModule.forRoot({
      config: {
         tokenGetter,
         whitelistedDomains: ['localhost:50297'],
         blacklistedRoutes: ['localhost:50297/api/auth']
      }
   }),
  ],
  providers: [
    SpinnerService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
