import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Product } from '../../model/product';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { ProductService } from '../../service/product.service';
import { CreateProductComponent } from '../create-product/create-product.component';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  spinnerName = 'listSpinner';
  products: Product[];
  bsModalRef: BsModalRef;

  constructor(private alertify: AlertifyService,
    private productService: ProductService,
    private router: Router,
    private spinner: SpinnerService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.loadProduct();
  }

  loadProduct() {
    this.spinner.showSpinner(this.spinnerName);
    this.productService.getProduct().subscribe((data: Product[]) => {
      this.products = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load product !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }

  createProduct() {
    this.bsModalRef = this.modalService.show(CreateProductComponent,  {
      initialState: {
        title: 'Create Product',
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.productToCreate.subscribe((product: Product) => {
      const productToCreate = product;
      if (productToCreate) {
        productToCreate.productId = 0;
        this.productService.createProduct(productToCreate).subscribe(() => {
          this.alertify.success('Product created successfully');
          this.loadProduct();
        }, error => {
          this.alertify.error('Failed to create product!');
          console.log(error);
        });
      }
    });
  }

  editProduct(product: Product) {
    this.bsModalRef = this.modalService.show(CreateProductComponent,  {
      initialState: {
        title: 'Update Product',
        product
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.productToCreate.subscribe((updatedProduct: Product) => {
      const productToCreate = updatedProduct;
      if (productToCreate) {
        this.productService.createProduct(productToCreate).subscribe(() => {
          this.alertify.success('Product update successfully');
          this.loadProduct();
        }, error => {
          this.alertify.error('Failed to update product!');
          console.log(error);
        });
      }
    });
  }

  deleteProduct(id: number) {
    this.alertify.confirm('Are you sure you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.productService.deleteProduct(id).subscribe(() => {
        this.alertify.success('item deleted successfully');
        this.loadProduct();
      }, error => {
        this.alertify.error('Failed to delete item');
        console.log(error);
      });
    });
  }

}
