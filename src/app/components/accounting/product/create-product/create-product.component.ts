import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from '../../model/product';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})

export class CreateProductComponent implements OnInit {
  productForm: FormGroup;
  product: Product;
  spinnerName = 'createSpinner';
  title: string;
  
  @Output() productToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
    public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createProductForm();
  }

  createProductForm() {
    if (this.product) {
      this.productForm = this.fb.group({
        productId: [this.product.productId],
        productName: [this.product.productName, Validators.required],
        price: [this.product.price, Validators.required],
        description: [this.product.description, Validators.required]
      });
    } else {
      this.productForm = this.fb.group({
        productId: ['0'],
        productName: [null, Validators.required],
        price: [null, Validators.required],
        description: [null, Validators.required]
      });
    }
  }

  onSubmit(): void {
    this.productToCreate.emit(this.productForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void {
    this.productForm.reset();
  }

}
