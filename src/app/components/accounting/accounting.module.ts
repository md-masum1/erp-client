import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountingRoutingModule } from './accounting-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListProductComponent } from './product/list-product/list-product.component';
import { CreateProductComponent } from './product/create-product/create-product.component';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    AccountingRoutingModule,
    SharedModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    ListProductComponent,
    CreateProductComponent
  ],
  entryComponents: [
    CreateProductComponent
  ]
})
export class AccountingModule { }
