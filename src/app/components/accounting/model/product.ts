export interface Product {
    productId: number;
    productName: number;
    price: string;
    description: string;
}