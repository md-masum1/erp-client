import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductComponent } from './product/list-product/list-product.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'product',
        component: ListProductComponent,
        data: {
          title: 'Product',
          breadcrumb: 'Product'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule { }