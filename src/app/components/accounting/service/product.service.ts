import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  baseUrl = environment.apiUrl + 'accounting/';

constructor(private http: HttpClient) { }

  getProduct() {
    return this.http.get(this.baseUrl + 'product');
  }

  createProduct(product: Product) {
    return this.http.post(this.baseUrl + 'product', product);
  }

  deleteProduct(id: number) {
    return this.http.delete(this.baseUrl + 'product/' + id);
  }

}
