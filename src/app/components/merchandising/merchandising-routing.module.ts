import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSeasonComponent } from './season/list-season/list-season.component';

const routes: Routes = [ 
    {
        path: '',
        children: [
            {
                path: 'season',
                component: ListSeasonComponent,
                data : {
                    title: 'Season',
                    breadcrumb: 'Season'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MerchandisingRoutingModule{  }