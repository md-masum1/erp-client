import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Season } from '../models/seasonmodel';


@Injectable({
    providedIn: 'root'
})

export class SeasonService {
    baseUrl = environment.apiUrl + 'merchandising/';

    constructor(private http: HttpClient) { }

    getSeason() {
        return this.http.get(this.baseUrl + 'season');
    }

    createSeason(season: Season) {
        return this.http.post(this.baseUrl + 'season', season);
    }

    deleteSeason(id: number) {
        return this.http.delete(this.baseUrl + 'season/' + id);
    }

}
