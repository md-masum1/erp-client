import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';

import { SharedModule } from 'src/app/shared/shared.module';

import { ListSeasonComponent } from './season/list-season/list-season.component';
import { CreateSeasonComponent } from './season/create-season/create-season.component';
import { MerchandisingRoutingModule } from './merchandising-routing.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MerchandisingRoutingModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    CreateSeasonComponent,
    ListSeasonComponent
  ],
  entryComponents:[
    CreateSeasonComponent
  ],
  exports:[

  ]
})
export class MerchandisingModule { }
