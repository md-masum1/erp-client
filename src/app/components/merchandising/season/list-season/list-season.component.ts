import { Component, OnInit } from '@angular/core';
import { Season } from '../../models/seasonmodel';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { SeasonService } from '../../services/seasonservice';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { CreateSeasonComponent } from '../create-season/create-season.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-season',
  templateUrl: './list-season.component.html',
  styleUrls: ['./list-season.component.css']
})

export class ListSeasonComponent implements OnInit {

  spinnerName = 'listSpinner';
  seasons: Season[];
  bsModalRef: BsModalRef; 

  constructor( private alertify : AlertifyService, 
    private seasonService: SeasonService, 
    private router: Router,
    private spinner: SpinnerService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.loadSeason();
  }

  loadSeason() {
    this.spinner.showSpinner(this.spinnerName);
    this.seasonService.getSeason().subscribe((data: Season[]) => {
      this.seasons = data;
      this.spinner.hideSpinner(this.spinnerName);
    }, error => {
      this.alertify.error('Failed to load product !');
      this.spinner.hideSpinner(this.spinnerName);
    });
  }


  createSeason() {
    this.bsModalRef = this.modalService.show(CreateSeasonComponent, {
      initialState: {
        title: 'Create Season',
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.seasonToCreate.subscribe((season: Season) => {
      const seasonToCreate = season;
      if (seasonToCreate) {
        seasonToCreate.seasonId = 0;
        this.seasonService.createSeason(seasonToCreate).subscribe(() => {
          this.alertify.success('Season Created successfully');
          this.loadSeason();
        }, error => {
          this.alertify.error('Failed to create product!');
          console.log(error);
        });
      }
    });
  }


  editSeason(season: Season) {
    this.bsModalRef = this.modalService.show(CreateSeasonComponent, {
      initialState: {
        title: 'Update Season', season
      },
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg'
    });

    this.bsModalRef.content.seasonToCreate.subscribe((updatedSeason: Season) => {
      const seasonToCreate = updatedSeason;
      if (seasonToCreate) {
        this.seasonService.createSeason(seasonToCreate).subscribe(() => {
          this.alertify.success('Season updated successfully');
          this.loadSeason();
        }, error => {
          this.alertify.error('Failed to update product!');
          console.log(error);
        });
      }
    });
  }


  deleteSeason(id: number) {
    this.alertify.confirm('Are you sure, you want to delete this item', () => {
      this.spinner.showSpinner(this.spinnerName);
      this.seasonService.deleteSeason(id).subscribe(() => {
        this.alertify.success('Deleted successfully.');
        this.loadSeason();
      }, error => {
        this.alertify.error('Failed to delete.');
        console.log(error);
      });
    });
  }

}
