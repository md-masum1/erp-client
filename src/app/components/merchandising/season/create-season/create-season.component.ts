import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Season } from '../../models/seasonmodel';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-create-season',
  templateUrl: './create-season.component.html',
  styleUrls: ['./create-season.component.css']
})
export class CreateSeasonComponent implements OnInit {

  seasonForm : FormGroup;
  season: Season;
  spinnerName = 'createSpinner';
  title: string;

  @Output() seasonToCreate = new EventEmitter();

  constructor(private fb: FormBuilder,
    public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
    this.createSeasonForm();
  }


  createSeasonForm(){
    if(this.season)
    {
      this.seasonForm = this.fb.group({
        seasonId : [this.season.seasonId],
        seasonName: [this.season.seasonName, Validators.required]
      });
    }
    else{
      this.seasonForm = this.fb.group({
        seasonId: ['0'],
        seasonName: [null, Validators.required],

      });
    }
  }

  onSubmit(): void {
    this.seasonToCreate.emit(this.seasonForm.value);
    this.bsModalRef.hide();
  }

  onClear(): void{
    this.seasonForm.reset();
  }

}
