import { NgModule } from '@angular/core';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';

import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SharedModule } from '../../shared/shared.module';
import { AuthService } from 'src/app/shared/service/auth.service';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    AuthRoutingModule,
    NgbModule,
    CarouselModule,
    SharedModule
  ],
  providers:[
    AuthService
  ]
})
export class AuthModule { }
