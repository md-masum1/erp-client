import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/shared/service/auth.service';
import { AlertifyService } from 'src/app/shared/service/alertify.service';
import { Router } from '@angular/router';
import { SpinnerService } from 'src/app/shared/service/spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public registerForm: FormGroup;
  model: any = {};

  constructor(private formBuilder: FormBuilder,
    public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    private spinner: SpinnerService) {}

  owlcarousel = [
    {
      title: 'Welcome to Multikart',
      desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.',
    },
    {
      title: 'Welcome to Multikart',
      desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.',
    },
    {
      title: 'Welcome to Multikart',
      desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy.',
    }
  ]
  owlcarouselOptions = {
    loop: true,
    items: 1,
    dots: true
  };

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      userName: [''],
      password: ['']
    });
  }
  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      userName: [''],
      password: [''],
      confirmPassword: [''],
    })
  }


  ngOnInit() {
    if (this.authService.loggedIn()) {
      this.router.navigate(['/dashboard/default/']);
    }
    this.createLoginForm();
  }

  login() {
    this.spinner.showSpinner();
    this.authService.login(this.loginForm.value).subscribe(
      next => {
        this.alertify.success('Login successfully!');
      },
      error => {
        this.alertify.error('Failed to login!');
        this.spinner.hideSpinner();
      },
      () => {
        this.spinner.hideSpinner();
        this.router.navigate(['/dashboard/default/']);
      }
    );
  }

}
